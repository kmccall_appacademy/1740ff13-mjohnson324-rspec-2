def measure(repititions = 1)
  starting_time = Time.now
  repititions.times { proc.call }
  (Time.now - starting_time) / repititions
end
