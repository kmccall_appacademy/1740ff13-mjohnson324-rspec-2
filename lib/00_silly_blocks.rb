def reverser
  reversed_string = []
  string = proc.call
  string.split.each { |word| reversed_string << word.reverse }
  reversed_string.join(" ")
end

def adder(addition = 1)
  proc.call + addition
end

def repeater(repititions = 1)
  repititions.times do
    proc.call
  end
end
